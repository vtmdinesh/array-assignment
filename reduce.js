const reduceFunction = (array = [], testFun, startValue) => {

    if (typeof (startValue) === "undefined") {
        startValue = array[0]
    }
    else {
        startValue = testFun(startValue, array[0])
    }


    if (typeof (array) !== "object") {
        return "Invalid data Type"
    }
    else {
        const reduce = (array, testFun, startValue) => {

            for (let i = 1; i < array.length; i++) {
                startValue = testFun(startValue, array[i])
            }
            return startValue
        }

        let output = reduce(array, testFun, startValue)
        return output
    }
}




module.exports = reduceFunction
const reduceFunction = require("../reduce.js")

const items = require("../arrays.js").items

const testingFunction = (acc, curr) => {
    return acc * curr
}


// please enter start value and remove "/*" if u want to provide start value
let startValue = 2

const result = reduceFunction(items, testingFunction,/*startValue*/)


console.log(result)
const findFunction = (array=[],testFun = function(item){return item}) => {
    
   
    
    
    if (array.length === 0 || (typeof(array)!=="object")){
        return "Invalid data Type"
    }
    else {  

        const find = (array,fun) => {

        for (let i=0;i<array.length;i++){
            if(fun(array[i],i,array)){
                return array[i]
            }
        }
        return undefined

    }

    let output = find(array,testFun)
    return output
}
}




module.exports = findFunction
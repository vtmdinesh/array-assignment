const flattenFunction = (array = [], depth = Infinity) => {
    let isSubarray = true;
    let output;

    while (depth > 0 && isSubarray) {
        output = []
        for (let i = 0; i < array.length; i++) {
            element = array[i]
            if (Array.isArray(element) === true && depth > 0) {
                output.push(...element)
            } 
            else if (array[i] === undefined) {
                continue
            }
            else {
                output.push(element)
            }

            for (let j =0;j<output.length;j++){
                item = output[j]
                isSubarray = Array.isArray(item)
                if (isSubarray){
                    break;
                }
                else{
                    continue
                }
            }
        

        }
        array = output
        depth--

    }

    return array


}

module.exports = flattenFunction
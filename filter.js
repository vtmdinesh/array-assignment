const filterFunction = (array=[],testFun = function(item,index,array){return item}) => {
    
   
    let outputArr =[]
    
    if (array.length === 0 || (typeof(array)!=="object")){
        return "Invalid data Type"
    }
    else {  


       for (let i=0;i<array.length;i++){
            if(testFun(array[i],i,array) === true){
                outputArr.push(array[i])
            }
        }
        return outputArr

    }

   

}




module.exports = filterFunction
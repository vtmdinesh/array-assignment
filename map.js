const mapFunction = (array=[],testFun = function(item){return item} )=> {
    
        
    if (array.length === 0 || (typeof(array)!=="object")){
       return "Invalid data Type"
    }

    else {
        let outputArray = []
        for (let i = 0; i < array.length; i++) {
    
            let resultItem = testFun(array[i], i,array);
            outputArray.push(resultItem);
        }
        return outputArray;
    }
}


module.exports = mapFunction
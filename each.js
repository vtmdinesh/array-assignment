const eachFunction = (array = [], testFun = function (item) { return item }) => {

    if (array.length === 0 || (typeof (array) !== "object")) {
        return "Invalid data Type"
    }
    else {
            for (let i = 0; i < array.length; i++) {
                array[i] = testFun(array[i], i, array)
            }
            return array

        }
       
    }





module.exports = eachFunction
